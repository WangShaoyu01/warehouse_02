package library_management.book;

import java.util.List;
import java.util.ArrayList;
/**
 * 书架类：
 * 存放一本本具体的图书，程序操作的就是书架类
 */
public class BookList {
    // books存放了所有当前书架中的书，每个Book对象就是一本书的实体
    // 将books设置成静态的列表，让所有的对象共用这一个，才能进行操作
    private static List<Book> books = new ArrayList<>();

    // booksName存放了所有的书本名称 - 查询书籍等都是通过书名来操作
    // 将booksName也要设置成静态的
    private static List<String> booksName = new ArrayList<>();

    // 初始化books和booksName,默认将四大名著放入书架中
    // 使用static代码块初始化static变量
    static {
        books.add(new Book("三国演义","罗贯中",66.6,"小说"));
        books.add(new Book("红楼梦","曹雪芹",88.8,"小说"));
        books.add(new Book("水浒传","施耐庵",55.6,"小说"));
        books.add(new Book("西游记","吴承恩",88.6,"小说"));

        booksName.add("三国演义");
        booksName.add("红楼梦");
        booksName.add("水浒传");
        booksName.add("西游记");
    }

    /** 展示书籍 **/
    public void displayBooks() {
        // 遍历所有书本，打印即可
        //for(元素类型T  循环元素名O : 循环对象)
        for (Book book : books) {
            System.out.println(book);
        }
    }

    /** 书籍名查找 **/
    public boolean contains(String booksName) {
        // 返回值是true / false
        return BookList.booksName.contains(booksName);
    }
    //查找书名为bookName的Book，返回值为Book类型的对象
    public Book findBook(String bookName) {
        for(int i=0;i< books.size();i++){
            if(books.get(i).getBookName().equals(bookName))
                return books.get(i);
        }
        return null;
    }

    /** 将书籍Book添加到books和booksName中 **/
    public void add(Book book) {
        books.add(book);
        booksName.add(book.getBookName());
    }

    /** 将书籍Book删掉 **/
    //需要删除books和booksName两个列表中的信息
    public void remove(String bookName){
        books.remove(this.findBook(bookName));
        booksName.remove(bookName);
    }
}
