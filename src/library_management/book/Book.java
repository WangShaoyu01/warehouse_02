package library_management.book;
/**
 * 书本实体类：
 * 封装书本的信息、重写toString方法打印书籍信息
 */
public class Book {
    //书名
    private String bookName;
    //作者
    private String author;
    //价格
    private double price;
    //书籍类别
    private String type;
    //借阅状态。默认是false
    private boolean isBorrowed;

    //无参构造
    public Book() {
    }
    //有参构造
    public Book(String bookName, String author, double price, String type) {
        this.bookName = bookName;
        this.author = author;
        this.price = price;
        this.type = type;
    }

    //Setter修改器，只需要修改 price、type、借阅状态
    public void setPrice(double price) {
        this.price = price;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setBorrowed(boolean borrowed) {
        isBorrowed = borrowed;
    }

    //Getter访问器
    public String getBookName() {
        return bookName;
    }

    public String getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public String getType() {
        return type;
    }

    public boolean isBorrowed() {
        return isBorrowed;
    }

    //打印
    @Override
    public String toString() {
        return "Book{" +
                "bookName='" + bookName + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                ", type='" + type + '\'' +
                ", isBorrowed=" + isBorrowed +
                '}';
    }
}
