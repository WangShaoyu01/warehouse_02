package library_management.operation.impl;

import library_management.book.BookList;
import library_management.operation.IOperation;
import java.util.Scanner;

/**
 * 实现接口：删除书籍
 */
public class DelOperation implements IOperation {

    //重写接口中的抽象方法实现删除操作
    @Override
    public void work(BookList bookList) {
        System.out.print("请输入您要删除的书籍名：");
        Scanner input = new Scanner(System.in);
        String bookName = input.next();

        if(bookList.contains(bookName)){
            //把bookName的书籍从books和booksName中删掉
            bookList.remove(bookName);
            System.out.println("删除成功！");
        }else{
            System.out.println("目前馆内本就没有该书，不需要您删除！");
        }
    }
}
