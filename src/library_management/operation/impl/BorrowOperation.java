package library_management.operation.impl;

import library_management.book.BookList;
import library_management.operation.IOperation;
import java.util.Scanner;
/**
 * 实现接口：借阅书籍
 */
public class BorrowOperation implements IOperation {

    //重写接口中的抽象方法实现借阅操作
    @Override
    public void work(BookList bookList) {
        System.out.print("请输入您要借阅的书籍名：");
        Scanner input = new Scanner(System.in);
        String bookName = input.next();

        if(bookList.contains(bookName)){
            if(bookList.findBook(bookName).isBorrowed()){
                System.out.println("抱歉，这本书已经被借出了！");
            }else{
                bookList.findBook(bookName).setBorrowed(true);
                System.out.println("恭喜您借阅成功！");
            }
        }else{
            System.out.println("抱歉，目前馆内还没有该藏书！");
        }
    }
}
