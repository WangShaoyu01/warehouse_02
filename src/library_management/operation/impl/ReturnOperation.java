package library_management.operation.impl;

import library_management.book.BookList;
import library_management.operation.IOperation;

import java.util.Scanner;

/**
 * 实现接口：归还书籍
 */
public class ReturnOperation implements IOperation {

    //重写接口中的抽象方法实现还书操作
    @Override
    public void work(BookList bookList) {
        System.out.print("请输入您要归还的书籍名：");
        Scanner input = new Scanner(System.in);
        String bookName = input.next();

        if(bookList.contains(bookName)){
            if(bookList.findBook(bookName).isBorrowed()){
                bookList.findBook(bookName).setBorrowed(false);
                System.out.println("归还成功！欢迎您再来借阅。");
            }else{
                System.out.println("您搞错了吧！虽然我们图书馆有这本藏书，但是没有您的借阅记录。");
            }
        }else{
            System.out.println("您搞错了吧！我们图书馆没有这本藏书，这应该不是从这儿借走的。");
        }
    }
}
