package library_management.operation.impl;

import library_management.book.BookList;
import library_management.operation.IOperation;
import java.util.Scanner;

/**
 * 实现接口：查找书籍
 */
public class FindOperation implements IOperation {

    //重写接口中的抽象方法实现查找操作
    @Override
    public void work(BookList bookList) {
        System.out.print("请输入您要查找的书籍名：");
        Scanner input = new Scanner(System.in);
        String bookName = input.next();

        if(bookList.contains(bookName)){
            System.out.println("这就是该书籍的全部信息。");
            System.out.println(bookList.findBook(bookName).toString());
        }else{
            System.out.println("抱歉，目前馆内还没有该藏书！");
        }
    }
}
