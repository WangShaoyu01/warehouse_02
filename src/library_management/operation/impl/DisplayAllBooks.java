package library_management.operation.impl;

import library_management.book.BookList;
import library_management.operation.IOperation;
/**
 * 实现接口：展示所有书籍
 */
public class DisplayAllBooks implements IOperation {

    //重写接口中的抽象方法实现展示书籍操作
    @Override
    public void work(BookList bookList) {
        System.out.println("以下就是图书馆中的全部书籍：");
        bookList.displayBooks();
    }
}
