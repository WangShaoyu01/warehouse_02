package library_management.operation.impl;

import library_management.book.Book;
import library_management.book.BookList;
import library_management.operation.IOperation;
/**
 * 实现接口：添加书籍
 */
public class AddOperation implements IOperation {

    //重写接口中的抽象方法实现添加操作
    @Override
    public void work(BookList bookList) {
        System.out.println("您现在进行的添加书籍操作~");
        System.out.print("请输入书籍名称:");
        String booksName = input.next();
        // 判断一下当前书籍是否已经存在
        if (bookList.contains(booksName)) {
            System.out.println("书本已经存在，无需重复添加~");
            return;
        }
        System.out.print("请输入作者:");
        String author = input.next();
        System.out.print("请输入价格:");
        double price = input.nextDouble();
        System.out.print("请输入书籍类别:");
        String type = input.next();
        Book book = new Book(booksName,author,price,type);
        bookList.add(book);
        System.out.println("添加书籍成功~");
    }
}
