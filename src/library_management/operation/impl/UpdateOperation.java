package library_management.operation.impl;

import library_management.book.BookList;
import library_management.operation.IOperation;
import java.util.Scanner;

/**
 * 实现接口：更新书籍
 */
public class UpdateOperation implements IOperation {

    //重写接口中的抽象方法实现更新书籍操作
    @Override
    public void work(BookList bookList) {
        System.out.print("请输入您要更新的书籍名：");
        Scanner input = new Scanner(System.in);
        String bookName = input.next();

        if(bookList.contains(bookName)){
            System.out.println("已经为您找到需要更新的书籍，请更新该书籍信息：");
            System.out.print("价格：");
            bookList.findBook(bookName).setPrice(input.nextDouble());
            System.out.print("类型：");
            bookList.findBook(bookName).setType(input.next());
            System.out.println("借阅状态：'1'表示借出，'0'表示未借出");
            int a = input.nextInt();
            bookList.findBook(bookName).setBorrowed(a == 1);
            System.out.println("该书籍信息更新成功！");
        }else{
            System.out.println("目前馆内没有该藏书，请核实书名！");
        }
    }
}
