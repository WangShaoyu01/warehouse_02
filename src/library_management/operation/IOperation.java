package library_management.operation;

import java.util.Scanner;
import library_management.book.BookList;
/**
 * 操作书架的接口
 * 功能：增删改查-借书-还书
 * 接口中只有全局常量和抽象方法
 */
public interface IOperation {
    // 全局常量，所有接口的子类共享
    // static + final共同修饰
    Scanner input = new Scanner(System.in);

    // 在对应的书架类进行操作
    void work(BookList bookList);
}