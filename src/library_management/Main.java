package library_management;

import library_management.book.BookList;
import library_management.user.AdminUser;
import library_management.user.NormalUser;
import library_management.user.User;
import java.util.Scanner;

/**
 * 程序的入口
 */
public class Main {
    private static Scanner input = new Scanner(System.in);
    public static void main(String[] args) {

        //进入登录界面
        User user = login();
        //创建一个书架对象
        BookList bookList = new BookList();
        while (true) {
            int choice = user.menu();
            if (choice == -1) {
                System.out.println("ByeBye ~~");
                break;
            }
            user.doOperation(choice,bookList);
        }
    }

    public static User login() {
        System.out.print("请输入用户名:");
        String name = input.next();
        System.out.println("请选择您的角色，1表示普通用户，0表示管理员");
        int choice = input.nextInt();
        if (choice == 1) {
            return new NormalUser(name);
        }
        return new AdminUser(name);
    }
}