package library_management.user;

import library_management.book.BookList;
import library_management.operation.IOperation;

/**
 * 用户类 - 抽象类
 * 子类：普通用户、管理员
 */
public abstract class User {
    // 用户名
    protected String name;
    // 有权限操作的方法
    protected IOperation[] operations;
    // 菜单，只有具体的子类才知道菜单长啥样~~
    public abstract int menu();

    public void doOperation(int choice, BookList bookList) {
        // 根据用户选择的菜单选择相应的操作类
        operations[choice - 1].work(bookList);
    }
}
