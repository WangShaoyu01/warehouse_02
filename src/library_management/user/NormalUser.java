package library_management.user;

import library_management.operation.IOperation;
import library_management.operation.impl.*;
import java.util.Scanner;

/**
 * 普通用户菜单类
 */
public class NormalUser extends User{
    private Scanner input = new Scanner(System.in);
    public NormalUser(String name) {
        this.name = name;
        this.operations = new IOperation[] {
                // 书籍列表
                new DisplayAllBooks(),
                // 查询书籍
                new FindOperation(),
                // 借阅书籍
                new BorrowOperation(),
                // 还书
                new ReturnOperation()
        };
    }

    /**
     * 此处的返回值就表示用户选择菜单中的哪个选项
     * @return
     */
    @Override
    public int menu() {
        System.out.println("=======================================");
        System.out.println("欢迎" + this.name + "登录比特图书管理系统");
        System.out.println("1.显示当前的书籍列表");
        System.out.println("2.查询书籍");
        System.out.println("3.借阅书籍");
        System.out.println("4.归还书籍");
        System.out.println("-1.退出");
        System.out.print("请输入您的选择:");
        int choice = input.nextInt();
        if (choice == -1) {
            // 关闭输入
            input.close();
        }
        System.out.println("=======================================");
        return choice;
    }
}