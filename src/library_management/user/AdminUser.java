package library_management.user;

import library_management.operation.IOperation;
import library_management.operation.impl.*;
import java.util.Scanner;

/**
 * 管理员菜单类
 */
public class AdminUser extends User {

    private Scanner input = new Scanner(System.in);
    public AdminUser(String name) {
        this.name = name;
        this.operations = new IOperation[] {
                // 增加书籍
                new AddOperation(),
                // 删除书籍
                new DelOperation(),
                // 更新书籍
                new UpdateOperation(),
                // 查询书籍
                new FindOperation(),
                // 查看当前书籍列表
                new DisplayAllBooks()
        };
    }

    /**
     * 此处的返回值就表示用户选择菜单中的哪个选项
     * @return
     */
    @Override
    public int menu() {
        System.out.println("=======================================");
        System.out.println("欢迎" + this.name + "登录比特图书管理系统");
        System.out.println("1.添加书籍");
        System.out.println("2.删除书籍");
        System.out.println("3.更新书籍");
        System.out.println("4.查询书籍");
        System.out.println("5.展示当前书籍列表");
        System.out.println("-1.退出");
        System.out.print("请输入您的选择:");
        int choice = input.nextInt();
        if (choice == -1) {
            // 关闭输入
            input.close();
        }
        System.out.println("=======================================");
        return choice;
    }
}
