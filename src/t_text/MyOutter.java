package t_text;
/**
 * 内部类的泛型
 * @param <T>
 */
public class MyOutter<T> {
    //成员内部类
    public class Inner1{
        public void fun1(T t){
            System.out.println(t);
        }
    }
    //静态内部类
    public static class Inner2<T>{
        public void fun2(T t){
            System.out.println(t);
        }
    }
    //错误的写法
//    private static class Inner3{
//        public void fun2(T t){
//            System.out.println(t);
//        }
//    }
}

class TestInner {
    public static void main(String[] args) {
        //先创建外部类对象
        MyOutter<Integer> outter = new MyOutter<>();
        //再创建成员内部类对象
        MyOutter<Integer>.Inner1 inner1 = outter.new Inner1();
        inner1.fun1(123);

        //可以直接创建静态内部类的对象
        MyOutter.Inner2<String> inner2 = new MyOutter.Inner2<>();
        inner2.fun2("Hello");
    }
}
