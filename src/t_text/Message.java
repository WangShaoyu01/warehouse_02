package t_text;
/**
 * 泛型通配符
 */
public class Message<T> {
    private T msg;

    public T getMsg() {
        return msg;
    }

    public void setMsg(T msg) {
        this.msg = msg;
    }

    //测试泛型对象的静态方法
    //方法参数规定只接受String类型的Message对象
    public static void fun1(Message<String> msg){
        System.out.println(msg.getMsg());
    }

    //泛型通配符
    public static void fun2(Message<?> msg){
        System.out.println(msg.getMsg());
        //这里不能调用msg.setMsg(T msg)方法，因为根本无法确定传入对象的类型
    }

    //上限通配符
    public static void fun3(Message<? extends Number> msg){
        System.out.println(msg.getMsg());
        //这里不能调用msg.setMsg(T msg)方法，因为子类的类型无法相互转换
        //msg.setMsg(123);
    }

    //下限通配符
    public static void fun4(Message<? super String> msg){
        System.out.println(msg.getMsg());
        //这里能调用msg.setMsg(T msg)方法，因为子类转换为父类的类型
        msg.setMsg("hehhe");
    }

    //主方法
    public static void main(String[] args) {
        Message<String> msg1 = new Message<>();
        msg1.setMsg("Hello");
        fun1(msg1);
        fun2(msg1);

        Message<Integer> msg2 = new Message<>();
        msg2.setMsg(123);
        // 报错，因为fun1方法只接受String类型的对象
        // fun1(msg2);
        fun2(msg2);
    }
}