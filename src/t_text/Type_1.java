package t_text;

import java.lang.reflect.Field;

/**
 * 类型擦除
 * @param <T>
 */
public class Type_1<T> {
    public T val;
}

class Type_2<T extends Number> {
    public T val;
}

class TestType {
    public static void main(String[] args) throws Exception{

        Type_1<Integer> cls1 = new Type_1<>();
        Type_1<String> cls2 = new Type_1<>();
        //比较二者编译之后的类型
        // true 因为编译后统一被擦除为Object类型
        System.out.println(cls1.getClass() == cls2.getClass());
        Field field1 = cls1.getClass().getDeclaredField("val");
        System.out.println(field1.getType());
        Field field2 = cls2.getClass().getDeclaredField("val");
        System.out.println(field2.getType());

        //编译后统一被擦除为Number类型
        Type_2<Integer> cls3 = new Type_2<>();
        Type_2<Double> cls4 = new Type_2<>();
        Field field3 = cls3.getClass().getDeclaredField("val");
        System.out.println(field3.getType());
        Field field4 = cls4.getClass().getDeclaredField("val");
        System.out.println(field4.getType());
    }
}
