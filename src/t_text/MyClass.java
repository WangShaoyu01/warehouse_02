package t_text;
/**
 * 泛型的引用
 */
public class MyClass<T> {

    private T val;

    //返回值为泛型的普通方法
    public T getVal(){
        return this.val;
    }

    //参数为泛型的普通方法
    public void setVal(T val){
        this.val = val;
    }

    /**
     * 没有返回值的泛型方法
     * @param t 泛型参数
     * @param <T> 表示是一个泛型方法，这里的T和该类的T无关
     */
    public <T> void fun(T t){
        System.out.println(t);
    }
}

class Main {
    public static void main(String[] args) {
        MyClass<Integer> cls1 = new MyClass<>();
        cls1.setVal(10);
        System.out.println(cls1.getVal());

        MyClass<String> cls2 = new MyClass<>();
        cls2.setVal("Hello");
        System.out.println(cls2.getVal());
        //泛型方法
        cls2.fun(123);
    }
}
