package t_text;
/**
 * 泛型接口
 */
public interface INews<T> {
    T getVal(T t);
}

//子类实现接口时可以保留泛型
class NewsImpl1<T> implements INews<T> {
    @Override
    public T getVal(T t) {
        return t;
    }
}

//子类实现接口时也可以明确类型
class NewsImpl2 implements INews<String> {
    @Override
    public String getVal(String s) {
        return s;
    }
}

//测试两个子类
class Test {
    public static void main(String[] args) {
        NewsImpl1<Integer> newsImpl1 = new NewsImpl1<>();
        System.out.println(newsImpl1.getVal(123));

        NewsImpl2 newsImpl2 = new NewsImpl2();
        System.out.println(newsImpl2.getVal("Hello"));
    }
}