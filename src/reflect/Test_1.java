package reflect;
/**
 * 获取类的class对象
 */
public class Test_1 {
    public static void main(String[] args) throws Exception {
        //要获取Student类的class对象
        //1.直接通过类名.class
        Class cls1 = Student.class;
        //2.通过该类的任意实例化对象，调用getClass方法
        Class cls2 = new Student().getClass();
        //3.使用Class类提供的Class.forName()方法
        Class cls3 = Class.forName("reflect.Student");
        // true
        System.out.println(cls1 == cls2 && cls2 == cls3);

        //per1在编译期和运行时都是Person类型
        Person per1 = new Person();
        //per2在编译期是Person类型的，但在运行时是Student类型的
        Person per2 = new Student();
        System.out.println(per1.getClass());
        System.out.println(per2.getClass());
    }
}
