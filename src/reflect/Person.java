package reflect;
//Person类
public class Person {
    private String personName;
    public int personAge;

    public Person() {
        System.out.println("Person类的无参构造");
    }
    private Person(String personName) {
        System.out.println("Person类的有参构造");
        this.personName = personName;
    }

    private void test() {
        System.out.println("Person类的test方法，private权限");
    }

    public void testPerson() {
        System.out.println("Person类的testPerson方法，public权限");
    }

}
