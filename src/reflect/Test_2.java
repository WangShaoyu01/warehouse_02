package reflect;

import java.lang.reflect.Constructor;
import java.util.Arrays;

/**
 * Constructor类
 * 与构造方法相关的类
 */
public class Test_2 {
    public static void main(String[] args) throws Exception{

        // 1.先获取Student类的class对象
        Class<Student> cls = Student.class;

        // 2.调用class对象的newInstance方法产生Student类的实例
        // 默认调用该类的无参构造，当访问不到无参构造时，无法使用该方法
//        Student stu = cls.newInstance();
//        System.out.println(stu);

        // 2.通过其它构造方法来产生Student类的实例

        // Class类的getConstructors()：只能获取当前类的所有public权限的构造方法
        Constructor[] constructors = cls.getConstructors();
        System.out.println(Arrays.toString(constructors));
        // Class类的getDeclaredConstructors()：能获取当前类的所有权限的构造方法
        Constructor[] constructors1 = cls.getDeclaredConstructors();
        System.out.println(Arrays.toString(constructors1));

        //拿到一个无参构造的constructor对象
        Constructor constructor = cls.getDeclaredConstructor();
        //拿到一个有参构造的constructor对象
        Constructor constructor1 = cls.getDeclaredConstructor(String.class); // private
//        Constructor constructor2 = cls.getDeclaredConstructor(String.class, Integer.class);
//        Constructor constructor3 = cls.getDeclaredConstructor(String.class, Integer.class, String.class);
        // 破坏封装,仅限当前JVM进程中的这个constructor1可用
        constructor1.setAccessible(true);
        Student stu = (Student) constructor1.newInstance("张三");
        //Student{name='张三', age=0, country='null'}
        System.out.println(stu);
    }
}
