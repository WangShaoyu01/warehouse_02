package reflect;

import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * Field类
 * 与类中属性相关的类
 */
public class Test_4 {
    public static void main(String[] args) throws Exception{
        // 1.那反射对象
        Class<?> cls = new Student().getClass();
        // 2.获取该类的属性
//        // 可以拿到当前类以及其父类的所有public的属性
//        Field[] fields = cls.getFields();
//        // 可以拿到当前类的所有权限的属性，包括private权限
//        Field[] fields1 = cls.getDeclaredFields();

        // 获取指定的属性
        Field field = cls.getDeclaredField("name");
        field.setAccessible(true); // 因为name是private修饰的属性
        //name是成员属性，所以需要Student的实例化对象
        Student stu = (Student) cls.newInstance();
        // 设置属性值：set(具体对象, 属性值);
        field.set(stu, "张三");
        // 获取属性值：通过具体的Student对象获取name属性
        System.out.println(field.get(stu));
    }
}
