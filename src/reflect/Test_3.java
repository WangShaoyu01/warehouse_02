package reflect;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Method类
 * 与类中方法相关的类
 */
public class Test_3 {
    public static void main(String[] args) throws Exception{
        // 1.要使用反射，先拿到该类的class对象
        Class<?> cls = Class.forName("reflect.Student");
        // 2.获取该类的Method对象
        // 可以拿到当前类以及其父类的所有public权限的方法
//        Method[] methods = cls.getMethods();
        // 可以拿到当前类的所有权限的方法，包括private权限
//        Method[] methods1 = cls.getDeclaredMethods();

        // cls.getDeclaredMethod(方法名称，方法的参数类型)
        Method staticMethod = cls.getDeclaredMethod("staticFunc", String.class);
        //静态方法的调用不需要实例化对象
        //methodFunc.invoke(具体对象，方法参数);
        staticMethod.invoke(null, "abcde");

        Method methodFunc = cls.getDeclaredMethod("fun");
        // fun()方法是private的，需要破坏封装
        methodFunc.setAccessible(true);
        // fun()是成员方法，必须通过Student的对象来调用
        Student stu = (Student) cls.newInstance();
        //开始调用方法
        //methodFunc.invoke(具体对象，方法参数);
        methodFunc.invoke(stu);
    }
}
