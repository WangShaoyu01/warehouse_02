package reflect;
//Student类
public class Student extends Person {

    //属性
    private String name;
    public int age;
    String country;

    //无参构造
    public Student(){
        System.out.println("Student类的无参构造");
    }
    //有参构造
    private Student(String name) {
        this.name = name;
    }
    Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public Student(String name, int age, String country) {
        this.name = name;
        this.age = age;
        this.country = country;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", country='" + country + '\'' +
                '}';
    }

    private void fun() {
        System.out.println("Student类的fun方法，private权限");
    }

    public int getAge(int num) {
        System.out.println("Student类的getAge方法，public权限");
        return this.age + num;
    }

    public static void staticFunc(String text) {
        System.out.println(text);
        System.out.println("Student类的静态方法");
    }
}
