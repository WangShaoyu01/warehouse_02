package interface_text.subject;
/**
 * 测试类：代购
 */
public class Test {

    public static void main(String[] args) {

        ISubject subject = Platform.getInstance();
        subject.buyComputer();
    }
}
/**
 * 代理设计模式：基于接口的
 * 一个接口两个实现类，一个是真实业务类，另一个是辅助真是业务的代理类
 * 最终将真实业务传递给代理类来完成整个业务的实现
 */