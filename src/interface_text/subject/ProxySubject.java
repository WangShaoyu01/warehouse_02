package interface_text.subject;
/**
 * 代理类，辅助我们买电脑
 * 代理类中有买电脑的·辅助操作
 */
public class ProxySubject implements ISubject {

    private ISubject realSubject;

    public ProxySubject(ISubject subject) {
        this.realSubject = subject;
    }

    @Override
    public void buyComputer() {
        beforeBuy();
        // 付钱这个操作是由真实业务来处理的
        this.realSubject.buyComputer();
        afterBuy();
    }

    public void beforeBuy() {
        System.out.println("买飞机票去漂亮国~");
    }

    public void afterBuy() {
        System.out.println("再买飞机票回国，送上14+7大礼包(隔离)~~");
    }
}
