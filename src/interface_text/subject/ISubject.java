package interface_text.subject;
/**
 * 业务接口，你要干的事儿，你要买的东西
 */
public interface ISubject {
    // 买电脑是我们的核心业务
    void buyComputer();
}