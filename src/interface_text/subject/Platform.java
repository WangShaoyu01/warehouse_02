package interface_text.subject;
/**
 * 平台类：最终通过平台，将代理人和客户建立联系
 */
public class Platform {
    //平台下单类
    public static ISubject getInstance() {
        // 在代购平台上下单
        // 创建代理类，并传入真实的代购业务
        return new ProxySubject(new RealSubject());
    }
}