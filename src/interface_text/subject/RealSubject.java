package interface_text.subject;
/**
 * 真实业务类，真正付钱买电脑的那个人
 */
public class RealSubject implements ISubject {

    @Override
    public void buyComputer() {
        System.out.println("买个mac pro 15寸顶配，真香~");
    }
}