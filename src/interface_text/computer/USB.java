package interface_text.computer;
/**
 * USB接口
 */
public interface USB {
    // 安装驱动
    void setUp();
    // 设备正常工作
    void work();
}