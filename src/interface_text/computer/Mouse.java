package interface_text.computer;
/**
 * 鼠标实现USB接口
 */
public class Mouse implements USB{

    @Override
    public void setUp() {
        System.out.println("安装鼠标驱动中...");
    }

    @Override
    public void work() {
        System.out.println("鼠标正常工作");
    }
}