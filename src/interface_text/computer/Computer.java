package interface_text.computer;
/**
 * 电脑是USB接口的使用者
 */
public class Computer {
    // 电脑上的USB插口1
    public void plugIn1(USB port1) {
        port1.setUp();
        port1.work();
    }
    // 电脑上的USB插口2
    public void plugIn2(USB port2) {
        port2.setUp();
        port2.work();
    }
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.plugIn1(new Mouse());
        computer.plugIn2(new Keyboard());
    }
}