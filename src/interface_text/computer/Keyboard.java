package interface_text.computer;
/**
 * 键盘实现USB接口
 */
public class Keyboard implements USB{

    @Override
    public void setUp() {
        System.out.println("安装键盘驱动中...");
    }

    @Override
    public void work() {
        System.out.println("键盘正常工作~");
    }
}