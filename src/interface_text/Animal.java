package interface_text;
/**
 * 实现java.lang.Cloneable接口
 */
public class Animal implements Cloneable{

    private String name;

    //覆写Object类中的clone()方法
    @Override
    protected Animal clone() throws CloneNotSupportedException {
        return (Animal) super.clone();
    }

    public static void main(String[] args) throws CloneNotSupportedException {

        Animal animal1 = new Animal();
        animal1.name = "熊猫";
        Animal animal2 = animal1.clone();
        //只是拷贝一个，并不是同一个对象
        System.out.println(animal1 == animal2);
        System.out.println(animal2.name);
    }
}