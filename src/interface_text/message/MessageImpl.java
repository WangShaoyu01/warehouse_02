package interface_text.message;
/**
 * 实现接口
 */
public class MessageImpl implements IMessage,INews{

    @Override
    public String message() {
        return "hello bit";
    }

    @Override
    public void getNews() {
        System.out.println("hello news~");
    }

    public static void main(String[] args) {

        IMessage message = new MessageImpl();
        //因为message是父接口IMessage的引用，所以只能调用message()方法
        System.out.println(message.message());

        //想调用getNews()方法，就要进行父接口之间的转换
        INews news = (INews) message;
        //这不是向下转型
        news.getNews();
    }
}
