package interface_text.message;
/**
 * interface定义了IMessage
 */
public interface IMessage {
    // 全局常量
    int NUM = 100;
    // 抽象方法
    String message();
}