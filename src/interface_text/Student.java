package interface_text;
/**
 * 实现java.lang.Comparable接口
 */
import java.util.Arrays;

public class Student implements Comparable<Student> {

    private String name;
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // 此时根据年龄大小比较
    @Override
    public int compareTo(Student o) {
        if (this.age == o.age) {
            return 0;
        }else if (this.age < o.age) {
            return -1;
        }
        return 1;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public static void main(String[] args) {

        Student stu1 = new Student("小白",18);
        Student stu2 = new Student("小黑",20);
        Student stu3 = new Student("老姜",30);
        Student[] students = {stu3,stu1,stu2};
        // 数组排序算法,按照升序排序
        Arrays.sort(students);
        System.out.println(Arrays.toString(students));
    }

}