package interface_text.animal;
/**
 * 狗狗具备跑的行为
 */
public class Dog extends Animal implements IRun{

    public Dog(String name) {
        super(name);
    }

    @Override
    public void eat(String food) {
        System.out.println("狗狗" + this.name + "吃了"+ food);
    }

    @Override
    public void run() {
        System.out.println("狗狗" + this.name + "正在撒欢的跑~~");
    }
}