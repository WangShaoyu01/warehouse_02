package interface_text.animal;
/**
 * 动物行为的接口：跑
 */
public interface IRun {
    void run();
}