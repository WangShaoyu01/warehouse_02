package interface_text.animal;
/**
 * 测试类：实现继承和接口
 */
public class Test {
    public static void main(String[] args) {

        Dog dog = new Dog("小六");
        dog.eat("狗粮");
        dog.run();

        System.out.println("---------------");
        Frog frog = new Frog("小蛙");
        frog.eat("昆虫");
        frog.run();
        frog.swim();

        System.out.println("---------------");
        Goose goose = new Goose("凶猛的大鹅");
        goose.eat("小虾米");
        goose.run();
        goose.swim();
        goose.fly();
    }
}