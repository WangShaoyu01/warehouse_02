package interface_text.animal;
/**
 * 大鹅具备走、游、飞的行为
 */
public class Goose extends Animal implements IRun,ISwim,IFly {

    public Goose(String name) {
        super(name);
    }

    @Override
    public void eat(String food) {
        System.out.println(this.name + "吃了" + food);
    }

    @Override
    public void fly() {
        System.out.println(this.name + "正在小飞一下~~");
    }

    @Override
    public void run() {
        System.out.println(this.name + "扇着翅膀跑~~");
    }

    @Override
    public void swim() {
        System.out.println(this.name + "在水里游泳");
    }
}
