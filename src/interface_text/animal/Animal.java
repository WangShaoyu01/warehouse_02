package interface_text.animal;
/**
 * 定义动物抽象类
 */
public abstract class Animal {

    protected String name;

    public Animal(String name) {
        this.name = name;
    }
    //抽象方法：吃
    public abstract void eat(String food);
}