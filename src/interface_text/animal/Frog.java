package interface_text.animal;
/**
 * 青蛙具备跳和游的行为
 */
public class Frog extends Animal implements IRun,ISwim{

    public Frog(String name) {
        super(name);
    }

    @Override
    public void eat(String food) {
        System.out.println(this.name + "吃了" +food);
    }

    @Override
    public void run() {
        System.out.println(this.name + "正在河边跳");
    }

    @Override
    public void swim() {
        System.out.println(this.name + "正在蛙泳");
    }
}