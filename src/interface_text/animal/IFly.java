package interface_text.animal;
/**
 * 动物行为的接口：飞
 */
public interface IFly {
    void fly();
}