package inner_text.local_inner;
/**
 * 定义局部内部类
 */
public class Outer { //外部类

    private String str1 = "Outer类的str1";
    private String str2 = "Outer类的str2";

    //定义一个外部的方法
    public void print(){
        System.out.println("Outer类的print方法");
    }

    static{
        class Di{} //局部内部类
    }

    public void method(){

        class Inner{ //局部内部类
            private String str1 = "Inner类的str1";

            public void visitOuter(){
                System.out.println(str1);
                System.out.println(Outer.this.str1);
                System.out.println(str2);
                print(); //直接调用外部类的方法
            }
        }

        //局部内部类只能在定义它的方法或代码块中使用
        Inner in = new Inner();
        in.visitOuter();

    }
}
