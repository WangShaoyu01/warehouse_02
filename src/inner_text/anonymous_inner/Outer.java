package inner_text.anonymous_inner;
/**
 * 定义匿名内部类
 */
public class Outer {

    private String str1 = "Outer类的str1";
    private String str2 = "Outer类的str2";

    //匿名内部类
    InterDemo id = new InterDemo() {

        private String str1 = "匿名内部类的str1";

        @Override
        public void print() {
            System.out.println(str1);
            System.out.println(Outer.this.str1);
            System.out.println(str2);
        }
    };

    public void show(){
        id.print();
    }
}
