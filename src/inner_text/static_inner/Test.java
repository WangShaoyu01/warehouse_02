package inner_text.static_inner;
/**
 * 测试静态嵌套类
 */
public class Test {
    public static void main(String[] args) {
        //不需要通过生成外部类对象来生成静态嵌套类对象
        Outer.Inner inner = new Outer.Inner();
        inner.show2();
    }
}
