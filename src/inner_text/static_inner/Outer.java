package inner_text.static_inner;
/**
 * 定义静态嵌套类
 */
public class Outer {

    //定义两个成员变量，一个非静态，一个静态
    private String str1 = "Outer类的str1";
    private static String str2 = "Outer类的静态的str2";

    //静态成员方法
    public static void show1(){
        System.out.println("外部类的show方法");
        //new Inner().show2();
    }

    /** 静态嵌套类 **/
    //只能定义到类的成员位置，不能定义在方法或代码块中
    public static class Inner{
        private String str3 = "静态嵌套类的str1";
        private static String str4 = "静态嵌套类的静态str2";

        public void show2(){
            System.out.println(str3);
            System.out.println(str4);

            //不能访问外部的非静态成员
            //可以直接访问外部的静态成员
            System.out.println(str2);
            show1();
        }
    }

}
