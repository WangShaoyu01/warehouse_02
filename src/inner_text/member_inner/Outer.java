package inner_text.member_inner;
/**
 * 定义成员内部类
 */
public class Outer { //外部类

    private String str1 = "Outer类的str1";
    private String str2 = "Outer类的str2";

    //外部类可通过成员内部类的对象调用内部类private修饰的私有成员
    public void show1(){
        Inner i = new Inner();
        System.out.println(i.str1);
    }

    public class Inner { //内部类

        private String str1 = "Inner类的str1";

        public void show2(){
            //内部类可以直接调用外部类的成员
            System.out.println(str2);
            //当内部类和外部类的成员重名时，优先调用内部类中的成员
            System.out.println(str1);
            //重名时，可以通过 "外部类名".this.成员名 的方式调用外部类的成员
            System.out.println(Outer.this.str1);
        }
    }
}
