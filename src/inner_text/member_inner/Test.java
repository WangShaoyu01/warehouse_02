package inner_text.member_inner;
/**
 * 测试成员内部类
 */
public class Test {
    public static void main(String[] args) {
        //需要先创建外部类的对象
        Outer outer = new Outer();
        outer.show1();

        //创建内部类对象
        Outer.Inner inner = outer.new Inner();
        inner.show2();
    }
}
