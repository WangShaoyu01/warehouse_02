package lambda_text;
/**
 * Lambda表达式的四种情况写法
 */
public class TestLambda {
    public static void main(String[] args) {

        // 1.无返回值无参
        NonReturnNonParameter i1 = () -> System.out.println("无返回值无参");
        System.out.println("-----------------------");
        i1.text1();

        // 2.无返回值有参
        NonReturnHasParameter i2 = (name,age) -> {
            System.out.println("无返回值有参");
            System.out.println(name + "今年" + age + "岁了！");
        };
        System.out.println("-----------------------");
        i2.text2("小明", 15);

        // 3.有返回值无参
        HasReturnNonParameter i3 = () -> {
            System.out.println("有返回值无参");
            return 1;
        };
        System.out.println("-----------------------");
        System.out.println(i3.text3());
        //当方法体中只有返回值这一条代码时，可以省略return和{}，直接写一个返回值
        HasReturnNonParameter i5 = () -> 10;
        System.out.println(i5.text3());

        // 4.有返回值有参
        HasReturnHasParameter i4 = (t1, t2) -> {
            System.out.println("有返回值有参");
            System.out.println(t1 + ":");
            return t2;
        };
        System.out.println("-----------------------");
        System.out.println(i4.text4("小蛋", 20));
    }
}

//表示该接口只有一个抽象方法
@FunctionalInterface
interface NonReturnNonParameter {
    void text1();
    // default声明的普通方法
    default void fun(int n){
        System.out.println("这是一个普通方法" + n);
    }
}

interface NonReturnHasParameter {
    void text2(String name, int age);
}

interface HasReturnNonParameter {
    int text3();
}

interface HasReturnHasParameter {
    int text4(String name, int age);
}