package lambda_text;
/**
 * 实现接口的三种写法
 */
//定义一个接口
interface IMessage {
    void fun(String msg);
}
//实现一个接口
class MessageImpl implements IMessage {
    @Override
    public void fun(String msg) {
        System.out.println(msg);
    }
}

public class TestImpl {
    public static void main(String[] args) {

        //子类实现接口的写法
        IMessage msg1 = new MessageImpl();
        msg1.fun("子类实现接口的写法");

        //匿名内部类的写法
        IMessage msg2 = new IMessage() {
            @Override
            public void fun(String msg) {
                System.out.println(msg);
            }
        };
        msg2.fun("匿名内部类的写法");

        //Lambda表达式的写法
        IMessage msg3 = (msg) -> System.out.println(msg);
        msg3.fun("Lambda表达式的写法");
        IMessage msg4 = (t) -> {
            System.out.println(t);
            System.out.println("多行代码需要加{}");
            System.out.println("(t) 中写参数类型可以省略,参数名可以任意改");
        };
        msg4.fun("Lambda表达式写法2");
    }
}
