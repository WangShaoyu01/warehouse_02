package abstract_text;
/**
 * 抽象方法的测试类
 */
public class TestCaffeineDrinks {

    public static void main(String[] args) {

        //咖啡的制作
        CaffeineDrinks coffee = new Coffee();
        System.out.println("咖啡的制作:");
        coffee.prepareDrinks();
        System.out.println("--------------");

        //茶的制作
        CaffeineDrinks tea = new Tea();
        System.out.println("茶的制作:");
        tea.prepareDrinks();
        System.out.println("--------------");

        //奶茶的制作
        CaffeineDrinks milkTea = new MilkTea();
        System.out.println("奶茶的制作:");
        milkTea.prepareDrinks();
    }
}
