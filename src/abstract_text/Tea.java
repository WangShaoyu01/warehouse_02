package abstract_text;
/**
 * 星巴克泡茶法
 */
public class Tea extends CaffeineDrinks {

    //重写抽象类中的冲泡方法
    @Override
    public void brew() {
        System.out.println("steep tea bag");
    }

    //重写抽象类中的调料方法
    @Override
    public void addCondiments() {
        System.out.println("add lemon");
    }
}
