package abstract_text;
/**
 * 奶茶制作
 */
public class MilkTea extends CaffeineDrinks {

    //重写抽象类中的冲泡方法
    @Override
    public void brew() {
        System.out.println("brew tea and coffee bag");
    }

    //重写抽象类中的调料方法
    @Override
    public void addCondiments() {
        System.out.println("add milk and sugar");
    }
}