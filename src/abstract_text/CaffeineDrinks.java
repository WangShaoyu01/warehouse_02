package abstract_text;
/**
 * 抽象类：咖啡因饮品
 */
public abstract class CaffeineDrinks {
    // 制作流程是标准化的，封装到父类中
    // 子类只有使用权不能修改
    // 饮品的制作流程，用final修饰，表示不能被覆写
    public final void prepareDrinks() {
        //烧水
        boilWater();
        //冲泡
        brew();
        //倒入杯中
        pourInCup();

        if(isCustomerWantsCondiments()) {
            //添加调味料
            addCondiments();
        }
    }
    // 对于浸泡和加调味品来说，不同子类实现细节不同，延迟到子类中去具体实现
    /** 抽象方法 **/
    //冲泡的方式不同
    public abstract void brew();
    //添加的调味料不同
    public abstract void addCondiments();

    //烧水
    public void boilWater() {
        System.out.println("boiling water");
    }

    //倒入杯中
    public void pourInCup() {
        System.out.println("pouring into cup");
    }

    //顾客是否添加调味料，默认添加
    public boolean isCustomerWantsCondiments() {
        return true;
    }
}