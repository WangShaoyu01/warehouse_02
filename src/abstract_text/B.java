package abstract_text;
/**
 * A是抽象类
 */
abstract class A{
    public A(){
        this.print();
    }
    //抽象方法
    public abstract void print();
}

/**
 * B是A的子类
 */
public class B extends A{
    int num = 10;

    public B(int num){
        this.num = num;
    }

    @Override
    public void print() {
        System.out.println("num = " + num);
    }

    //测试
    public static void main(String[] args) {
        new B(100);
        /**
         * 先执行父类的构造方法
         * 从父类的抽象方法跳到子类中，执行重写后的print方法
         * 此时num还没赋值，所以num = 0
         */
    }
}
