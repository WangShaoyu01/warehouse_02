package abstract_text;
/**
 * 星巴克咖啡制作
 */
import java.util.Scanner;

public class Coffee extends CaffeineDrinks {

    Scanner input = new Scanner(System.in);

    //重写抽象类中的冲泡方法
    @Override
    public void brew() {
        System.out.println("brew coffee bag");
    }

    //重写抽象类中的调料方法
    @Override
    public void addCondiments() {
        System.out.println("add sugar and milk");
    }

    //重写抽象类中的是否调料方法
    @Override
    public boolean isCustomerWantsCondiments() {
        System.out.print("您的咖啡是否加糖和奶? y表示加，n表示不加 ：");
        String str = input.nextLine();
        if (str.equals("y")) {
            return true;
        }
        return false;
    }
}