package exception_text;
/**
 * 关键字tfrows和throw
 */
public class ThrowsTest {

    public static void main(String[] args) {
        try {
            test();
        }catch (NullPointerException e) {
            System.out.println("捕捉空指针异常！");
            e.printStackTrace();
        }
        System.out.println("------------------");
        fun();
    }

    //调用test()方法有可能产生空指针异常，但是test方法不处理该异常，谁调用谁处理
    public static void test() throws NullPointerException,ArrayIndexOutOfBoundsException {
        String str = null;
        System.out.println(str.length());
    }

    public static void fun() {
        //人为产生一个空指针异常对象，抛给调用者看
        throw new NullPointerException("抛出个空指针异常给调用者看！");
        //抛出异常后，方法就会结束
    }
}
