package exception_text;
/**
 * 异常处理
 */
public class ExceptionTest {

    public static void main(String[] args) {
        int[] arr = {10,20,30};
        try {
            // try中放可能出现异常的代码
            System.out.println(arr[10]);
            System.out.println("try中出现异常以后的代码就不会运行");
        }catch (NullPointerException e){
            // catch捕获符合类型的异常
            System.out.println("空指针异常！");
            //打印错误堆栈信息
            e.printStackTrace();
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("下标越界异常！");
            e.printStackTrace();
        }finally {
            //不管代码是否有异常，都会执行 finally 块中的代码
            System.out.println("finally 块中的代码");
        }
        System.out.println("异常处理之后的代码块");
        System.out.println("----------------------");
        System.out.println(testException());
    }

    //关于异常的返回值
    public static int testException() {
        try {
            String str = null;
            System.out.println(str.equals("123"));
            //若没有出现异常，返回1
            return 1;
        }catch (NullPointerException e) {
            System.out.println("空指针异常！");
            e.printStackTrace();
            //若出现该类型异常，返回2
            return 2;
        }finally {
            System.out.println("finally中的代码段");
            // return 3;
            //一旦finally 中带了返回值，就会覆盖try和catch中的返回值，
            // 因为无论有无异常都会执行finally中的代码
        }
    }
}
