package exception_text;
/**
 * 自定义异常类
 */
import java.util.Scanner;

public class CustomException {

    public static final String USER = "张三";
    public static final String PASSWORD = "666";

    public static void main(String[] args) {
        try {
            login();
            System.out.println("登录成功!");
        } catch (UserNameException e) {
            e.printStackTrace();
        } catch (PasswordException e) {
            e.printStackTrace();
        }
    }

    //登录
    public static void login() throws UserNameException,PasswordException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入用户名:");
        String name = scanner.next();
        System.out.print("请输入密码:");
        String password = scanner.next();
        if (!name.equals(USER)) {
            // 抛出用户名错误异常
            throw new UserNameException("用户名错误!");
        }
        if (!password.equals(PASSWORD)) {
            // 抛出密码错误异常
            throw new PasswordException("密码错误!");
        }
    }
}

// 用户名异常
class UserNameException extends Exception {
    public UserNameException(String msg) {
        super(msg);
    }
}

// 密码异常
class PasswordException extends RuntimeException {
    public PasswordException(String msg) {
        super(msg);
    }
}